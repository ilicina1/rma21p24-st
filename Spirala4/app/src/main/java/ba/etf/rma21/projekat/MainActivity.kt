package ba.etf.rma21.projekat

import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.data.kvizoviKorisnik
import ba.etf.rma21.projekat.data.models.Korisnik
import ba.etf.rma21.projekat.data.odgovorenaPitanja
import ba.etf.rma21.projekat.data.repositories.AccountRepository
import ba.etf.rma21.projekat.data.upisaniPredmeti
import ba.etf.rma21.projekat.data.upisaniPredmetiGrupe
import ba.etf.rma21.projekat.view.FragmentKvizovi
import ba.etf.rma21.projekat.view.FragmentPredmeti
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {
    private lateinit var bottomNavigation: BottomNavigationView

    //Listener za click
    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {

                R.id.kvizovi -> {
                    val fragmentKvizovi = FragmentKvizovi.newInstance()
                    openFragment(fragmentKvizovi)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.predmeti -> {
                    val fragmentPredmeti = FragmentPredmeti.newInstance()
                    openFragment(fragmentPredmeti)
                    return@OnNavigationItemSelectedListener true
                }

            }

            false
        }

    companion object {
        var korisnik: Korisnik =
            Korisnik(
                upisaniPredmeti(),
                upisaniPredmetiGrupe(),
                kvizoviKorisnik(),
                odgovorenaPitanja()
            )
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        val intent = intent
        GlobalScope.launch (Dispatchers.Main) {
            if(intent.getStringExtra("payload") != null) AccountRepository.postaviHash(intent.getStringExtra("payload")!!);
        }

//        intent.getStringExtra("payload")

        setContentView(R.layout.activity_main)


        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        bottomNavigation = findViewById(R.id.bottomNavigation)

        bottomNavigation.menu.findItem(R.id.predajKviz).isVisible = false
        bottomNavigation.menu.findItem(R.id.zaustaviKviz).isVisible = false

        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottomNavigation.selectedItemId = R.id.kvizovi

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // Handle the back button event
                if (bottomNavigation.selectedItemId == R.id.kvizovi) {
                    bottomNavigation.selectedItemId = R.id.predmeti
                } else if (bottomNavigation.selectedItemId == R.id.predmeti) {
                    bottomNavigation.selectedItemId = R.id.kvizovi
                }
            }
        }

        getOnBackPressedDispatcher().addCallback(this, callback);
    }


    //Funkcija za izmjenu fragmenta
    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}