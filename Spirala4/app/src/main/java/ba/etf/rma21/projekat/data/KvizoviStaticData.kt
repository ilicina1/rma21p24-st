package ba.etf.rma21.projekat.data

import ba.etf.rma21.projekat.data.models.*
import java.util.*

val dateMinusTwoMonths = Calendar.getInstance().run {
    add(Calendar.MONTH, -2)
    time
}

val dateMinusOneMonth = Calendar.getInstance().run {
    add(Calendar.MONTH, -1)
    time
}

val datePlusOneMonth = Calendar.getInstance().run {
    add(Calendar.MONTH, 1)
    time
}

val dateToday = Calendar.getInstance().run {
    add(Calendar.MONTH, 0)
    time
}


fun kvizovi(): List<Kviz> {
    val cal = Calendar.getInstance()
    cal[Calendar.YEAR] = 2021
    cal[Calendar.MONTH] = Calendar.APRIL
    cal[Calendar.DAY_OF_MONTH] = 20
    val date = cal.time

    val calNew = Calendar.getInstance()
    calNew[Calendar.YEAR] = Calendar.getInstance().get(Calendar.YEAR)
    calNew[Calendar.MONTH] = Calendar.getInstance().get(Calendar.MONTH)
    calNew[Calendar.DAY_OF_MONTH] = Calendar.getInstance().get(Calendar.DAY_OF_MONTH - 1)
    val dateNew = calNew.time
    return listOf(
//        Kviz(
//            1,"Kviz 1", "IF1", date, Date(1620597600000),
//            null, 5, "Grupa-2", 0f
//        ),
//        Kviz(
//            2,"Kviz 2", "ASP", Date(), Date(1620597600000),
//            Date(), 2, "Grupa-1", 15f
//        ),
//        Kviz(
//            3,"Kviz 3", "WT", Date(1612911600000), Date(1615330800000),
//            Date(1612911600000), 6, "Grupa-2", 21f
//        ),
//        Kviz(
//            4,"Kviz 4", "OIS", Date(1612911600000), Date(1615330800000),
//            null, 2, "Grupa-1", 0f
//        ),
//        Kviz(
//            5,"Kviz 5", "ICR", dateNew, Date(1620597600000),
//            null, 2, "Grupa-1", 0f
//        ),
//        Kviz(
//            6,"Kviz 6", "ASP", Date(1630533600000), Date(1633125600000),
//            null, 5, "Grupa-2", 0f
//        ),
//        Kviz(
//            7,"Kviz 7", "IF1", Date(1609542000000), Date(1614553200000),
//            Date(1612911600000), 2, "Grupa-1", 15f
//        ),
//        Kviz(
//            8,"Kviz 8", "OIS", Date(1599861600000), Date(1605913200000),
//            null, 6, "Grupa-2", 0f
//        ),
//        Kviz(
//            9,"Kviz 9", "WT", Date(1587420000000), Date(1590012000000),
//            null, 2, "Grupa-1", 0f
//        ),
//        Kviz(
//            10,"Kviz 10", "ICR", Date(1617919200000), Date(1624831200000),
//            null, 5, "Grupa-2", 0f
//        ), Kviz(
//            11,"Kviz 11", "RPR", Date(1617919200000), Date(1624831200000),
//            null, 5, "Grupa-2", 0f
//        ),
//        Kviz(
//            12,"Kviz 12", "IM1", Date(1614466800000), Date(1614985200000),
//            Date(1614639600000), 2, "Grupa-1", 15f
//        ),
//        Kviz(
//            13,"Kviz 13", "USU", Date(1610838000000), Date(1613775600000),
//            Date(1611874800000), 6, "Grupa-2", 21f
//        ),
//        Kviz(
//            14,"Kviz 14", "UK", Date(1634940000000), Date(1635807600000),
//            null, 2, "Grupa-1", 0f
//        ),
//        Kviz(
//            15,"Kviz 15", "DOS", Date(1617919200000), Date(1624831200000),
//            null, 2, "Grupa-1", 0f
//        ),
//        Kviz(
//            16,"Kviz 16", "IM1", Date(1643497200000), Date(1643842800000),
//            null, 5, "Grupa-2", 0f
//        ),
//        Kviz(
//            17,"Kviz 17", "RPR", Date(1588543200000), Date(1592085600000),
//            null, 2, "Grupa-1", 0f
//        ),
//        Kviz(
//            18,"Kviz 18", "UK", Date(1588543200000), Date(1592172000000),
//            Date(1589925600000), 6, "Grupa-2", 21f
//        ),
//        Kviz(
//            19,"Kviz 19", "USU", Date(1597874400000), Date(1603144800000),
//            Date(1600639200000), 2, "Grupa-1", 6f
//        ),
//        Kviz(
//            20,"Kviz 20", "DOS", Date(1597874400000), Date(1603144800000),
//            Date(1601330400000), 5, "Grupa-2", 13f
//        )
    )
}

fun kvizoviKorisnik(): List<Kviz> {
    val cal = Calendar.getInstance()
    cal[Calendar.YEAR] = 2021
    cal[Calendar.MONTH] = Calendar.APRIL
    cal[Calendar.DAY_OF_MONTH] = 20
    val date = cal.time

    val calNew = Calendar.getInstance()
    calNew[Calendar.YEAR] = Calendar.getInstance().get(Calendar.YEAR)
    calNew[Calendar.MONTH] = Calendar.getInstance().get(Calendar.MONTH)
    calNew[Calendar.DAY_OF_MONTH] = Calendar.getInstance().get(Calendar.DAY_OF_MONTH - 1)
    val dateNew = calNew.time

    return listOf(
//        kvizovi().get(0), kvizovi().get(1)

    )
}

fun predmeti(): List<Predmet> {
    return listOf(
//        Predmet(
//            1,"IF1", 1
//        ),
//        Predmet(
//            2,"IM1", 1
//        ),
//        Predmet(
//            3,"ASP", 2
//        ),
//        Predmet(
//            4,"RPR", 2
//        ),
//        Predmet(
//            5,"WT", 3
//        ),
//        Predmet(
//            6,"OIS", 3
//        ),
//        Predmet(
//            7,"DOS", 4
//        ),
//        Predmet(
//            8,"USU", 4
//        ),
//        Predmet(
//            9,"UK", 5
//        ),
//        Predmet(
//            10,"ICR", 5
//        )
    )
}

fun grupe(): List<Grupa> {
    return listOf(
//        Grupa(
//            1, "Grupa-1", "IF1"
//        ),
//        Grupa(
//            2,"Grupa-2", "IF1"
//        ),
//        Grupa(
//            3,"Grupa-1", "IM1"
//        ),
//        Grupa(
//            4,"Grupa-2", "IM1"
//        ),
//        Grupa(
//            5,"Grupa-1", "ASP"
//        ),
//        Grupa(
//            6,"Grupa-2", "ASP"
//        ),
//        Grupa(
//            7,"Grupa-1", "RPR"
//        ),
//        Grupa(
//            8,"Grupa-2", "RPR"
//        ),
//        Grupa(
//            9,"Grupa-1", "WT"
//        ),
//        Grupa(
//            10,"Grupa-2", "WT"
//        ),
//        Grupa(
//            11,"Grupa-1", "OIS"
//        ),
//        Grupa(
//            12,"Grupa-2", "OIS"
//        ),
//        Grupa(
//            13,"Grupa-1", "DOS"
//        ),
//        Grupa(
//            14,"Grupa-2", "DOS"
//        ),
//        Grupa(
//            15,"Grupa-1", "USU"
//        ),
//        Grupa(
//            16,"Grupa-2", "USU"
//        ),
//        Grupa(
//            17,"Grupa-1", "UK"
//        ),
//        Grupa(
//            18,"Grupa-2", "UK"
//        ),
//        Grupa(
//            19,"Grupa-1", "ICR"
//        ),
//        Grupa(
//            20,"Grupa-2", "ICR"
//        )
    )
}

fun upisaniPredmeti(): List<Predmet> {
    return listOf(
        Predmet(
            1,"IF1", 1
        ),
        Predmet(
            2,"ASP", 2
        )
    )
}

fun upisaniPredmetiGrupe(): List<Grupa> {
    return listOf(
//        Grupa(
//            1,"Grupa-1", "IF1"
//        ),
//        Grupa(
//            2,"Grupa-1", "ASP"
//        )
    )
}

fun odgovorenaPitanja(): List<OdgovorenaPitanja>{
    return listOf(

    )
}



fun pitanja(): List<Pitanje> {
    return listOf(
//        Pitanje(1, "Pitanje neko", "Pitanje sta je lorem", listOf<String>("aa", "bb", "cc", "dd", "ee"), 1),
//        Pitanje(2, "Pitanje neko drugo", "Pitanje sta je ipsum", listOf<String>("aa", "fa", "cfafc", "dd", "ee"), 0),
//        Pitanje(3, "Pitanje neko lijevo", "Pitanje sta je dolor", listOf<String>("aasdaa", "bb", "cc", "dd", "ee"), 3),
//        Pitanje(4, "Pitanje neko desno", "Pitanje sta je emet", listOf<String>("aa", "bb", "cc", "dd", "eeddd"), 1),
//        Pitanje(5, "Pitanje neko ono", "Pitanje sta je zakat", listOf<String>("aa", "sdddbb", "cc", "dd", "ee"), 3),
//        Pitanje(6, "Pitanje neko ovo", "Pitanje sta je reket", listOf<String>("aa", "bb", "cc", "dd", "ee"), 0),
//        Pitanje(7, "Pitanje eto nako", "Pitanje sta je vakat", listOf<String>("qqq", "bwwwb", "cc", "dd", "ee"), 1),
//        Pitanje(8, "Pitanje eto ovo", "Pitanje sta je lakat", listOf<String>("aa", "bbqq", "cc", "dd", "ee"), 2),
//        Pitanje(9,"Pitanje eto ono", "Pitanje sta je blabla", listOf<String>("aa", "bb", "cc", "dd", "ee"), 1),
//        Pitanje(10,"Pitanje bla bla", "Pitanje sta je nana", listOf<String>("aa", "bb", "cc", "dd", "ee"), 2),
//        Pitanje(11,"Pitanje bla nana", "Pitanje sta je nanakava", listOf<String>("aa", "bb", "cc", "dd", "ee"), 1),
//        Pitanje(12,"Pitanje bla bababa", "Pitanje sta je rarara", listOf<String>("aa", "bb", "cc", "dd", "ee"), 0),
//        Pitanje(13,"Pitanje bla laalalal", "Pitanje sta je dadada", listOf<String>("aa", "bb", "cc", "dd", "ee"), 3)

    )
}

fun pitanjaKviz(): List<PitanjeKviz> {
    return listOf(
        PitanjeKviz("Pitanje neko drugo", "Kviz 2"),
        PitanjeKviz("Pitanje neko", "Kviz 2"),
        PitanjeKviz("Pitanje neko lijevo", "Kviz 2"),
        PitanjeKviz("Pitanje eto nako", "Kviz 2"),
        PitanjeKviz("Pitanje eto ovo", "Kviz 1"),
        PitanjeKviz("Pitanje eto ono", "Kviz 1"),
        PitanjeKviz("Pitanje bla bla", "Kviz 1"),
        PitanjeKviz("Pitanje neko desno", "Kviz 3"),
        PitanjeKviz("Pitanje neko ono", "Kviz 3"),
        PitanjeKviz("Pitanje neko ovo", "Kviz 4"),
        PitanjeKviz("Pitanje bla nana", "Kviz 4"),
        PitanjeKviz("Pitanje bla bababa", "Kviz 5"),
        PitanjeKviz("Pitanje bla laalalal", "Kviz 5")
    )
}