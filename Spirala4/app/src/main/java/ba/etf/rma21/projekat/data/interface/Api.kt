package ba.etf.rma21.projekat.data.`interface`

import ba.etf.rma21.projekat.data.models.*
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path


interface Api {
    @GET("/kviz/{id}/pitanja")
    suspend fun getPitanja(@Path("id") id: Int?): Response<List<Pitanje>>

    @POST("/student/ef565047-e5ba-4117-a205-8dd9fa9a883a/kviz/{kid}")
    suspend fun getKvizTaken( @Path("kid") kid: Int?): Response<KvizTaken>

    @GET("/student/ef565047-e5ba-4117-a205-8dd9fa9a883a/kviztaken")
    suspend fun getListKvizTaken(): Response<List<KvizTaken>>

    @GET("/kviz")
    suspend fun getKvizAll(): Response<List<Kviz>>

    @GET("/kviz/{id}")
    suspend fun getKvizById(@Path("id") id: Int?): Response<Kviz>

    @GET("/grupa/{id}/kvizovi")
    suspend fun getUpisani(@Path("id") id: Int?): Response<List<Kviz>>

    @GET("/student/ef565047-e5ba-4117-a205-8dd9fa9a883a/grupa")
    suspend fun getStudentGrupa(): Response<List<Grupa>>

    @GET("/student/ef565047-e5ba-4117-a205-8dd9fa9a883a/kviztaken/{id}/odgovori")
    suspend fun getOdgovoriKviz(@Path("id") id: Int?): Response<List<Odgovor>>

    @GET("/predmet")
    suspend fun getPredmeti(): Response<List<Predmet>>

    @GET("/grupa")
    suspend fun getGrupe(): Response<List<Grupa>>

    @POST("/grupa/{gid}/student/ef565047-e5ba-4117-a205-8dd9fa9a883a")
    suspend fun upisiUGrupu(@Path("gid") gid: Int?): Response<Kviz>

    @GET("/student/ef565047-e5ba-4117-a205-8dd9fa9a883a/grupa")
    suspend fun getUpisaneGrupe(): Response<List<Grupa>>

    @GET("/predmet/{id}/grupa")
    suspend fun  getGrupeZaPredmet(@Path("id") id: Int?): Response<List<Grupa>>

    @POST("/student/ef565047-e5ba-4117-a205-8dd9fa9a883a/kviztaken/{ktid}/odgovor")
    suspend fun postOdgovor(@Path("ktid") ktid: Int?, @Body body: OdgovorPost? ): Response<KvizTaken>
}