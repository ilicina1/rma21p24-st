package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class Account (
    @PrimaryKey @SerializedName("acHash")  val acHash: String,
    @ColumnInfo(name = "lastUpdate") @SerializedName("lastUpdate") val lastUpdate: String

) : Serializable {

}