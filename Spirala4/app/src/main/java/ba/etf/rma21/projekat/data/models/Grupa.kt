package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class Grupa(
    @PrimaryKey @SerializedName("id")  val id: Int,
    @ColumnInfo(name = "naziv") @SerializedName("naziv") val naziv: String,
    var nazivPredmeta: String

) : Serializable {

}