package ba.etf.rma21.projekat.data.models

import java.io.Serializable

class Korisnik(var upisaniPredmeti: List<Predmet>, var upisaniPredmetiGrupe: List<Grupa>, var kvizovi: List<Kviz>, var odgovorenaPitanja: List<OdgovorenaPitanja>) : Serializable  {
}