package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class Kviz(
    @PrimaryKey @SerializedName("id") val id: Int,
    @ColumnInfo(name = "naziv") @SerializedName("naziv") val naziv: String,
//    val nazivPredmeta: String,
    @ColumnInfo(name = "datumPocetka") @SerializedName("datumPocetka") val datumPocetka: String,
    @ColumnInfo(name = "datumKraj") @SerializedName("datumKraj") val datumKraj: String,
//    val datumRada: Date?,
    @ColumnInfo(name = "trajanje") @SerializedName("trajanje")  val trajanje: Int
//    val nazivGrupe: String,
//    val osvojeniBodovi: Float
) : Serializable{

}