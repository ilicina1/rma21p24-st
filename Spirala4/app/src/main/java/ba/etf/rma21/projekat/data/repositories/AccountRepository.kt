package ba.etf.rma21.projekat.data.repositories

import android.content.Context

class AccountRepository {

    companion object {
        var acHash: String = "ef565047-e5ba-4117-a205-8dd9fa9a883a"
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }
        suspend fun postaviHash(acHash: String): Boolean {
            this.acHash = acHash
            return true
        }

        fun getHash(): String {
            return acHash
        }


    }
}