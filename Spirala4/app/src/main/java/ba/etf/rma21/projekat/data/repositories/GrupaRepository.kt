package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.grupe
import ba.etf.rma21.projekat.data.models.Grupa

class GrupaRepository {
    companion object {
        init {

        }

        fun getGroupsByPredmet(nazivPredmeta: String): List<Grupa> {
            var pom = listOf<Grupa>()
            for (i in grupe()) {
                if (i.nazivPredmeta == nazivPredmeta)
                    pom += i
            }
            return pom
        }

        fun getGroupsByPredmetString(nazivPredmeta: String): List<String> {
            var pom = listOf<String>()
            for (i in grupe()) {
                if (i.nazivPredmeta == nazivPredmeta)
                    pom += i.naziv
            }
            return pom
        }

    }
}