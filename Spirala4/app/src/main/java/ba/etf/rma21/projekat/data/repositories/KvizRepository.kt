package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.data.kvizovi
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.upisaniPredmeti
import ba.etf.rma21.projekat.view.ApiAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class KvizRepository {

    companion object {

        init {

        }

        suspend fun getById(id:Int):Kviz? {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getKvizById(id);
                return@withContext response.body()
            }
        }

        suspend fun getUpisani():List<Kviz>? {
            var response = ApiAdapter.retrofit.getStudentGrupa();
            var finalList: List<Kviz>? = listOf<Kviz>();

            for( i in response.body()!!){
                var response2 = ApiAdapter.retrofit.getUpisani(i.id);
                finalList = response2.body()
            }

            return finalList
        }

        fun getMyKvizes(): List<Kviz> {
            var myKvizes = listOf<Kviz>()

            return MainActivity.korisnik.kvizovi
        }

        suspend fun getAll(): List<Kviz>? {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getKvizAll();
                return@withContext response.body()
            }
        }

        fun getAlla(): List<Kviz> {
            return kvizovi()
        }

        fun getDone(): List<Kviz> {
            var myKvizes = listOf<Kviz>()

            for (i in MainActivity.korisnik.kvizovi) {
//                if (i.datumRada != null)
//                    myKvizes += i
            }

            return myKvizes
        }

        fun getFuture(): List<Kviz> {
            var myKvizes = listOf<Kviz>()

            for (i in MainActivity.korisnik.kvizovi) {
//                if (i.datumPocetka.after(Date()))
//                    myKvizes += i
            }
            return myKvizes
        }

        fun getNotTaken(): List<Kviz> {
            var myKvizes = listOf<Kviz>()

            for (i in MainActivity.korisnik.kvizovi) {
//                if (i.datumKraj.before(Date()) && i.datumRada == null)
//                    myKvizes += i
            }
            return myKvizes
        }

    }
}