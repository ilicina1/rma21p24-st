package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.OdgovorPost
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.view.ApiAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class OdgovorRepository {
    companion object {
        private lateinit var context: Context
        fun setContext(_context:Context){
            context=_context
        }
        suspend fun getOdgovoriKviz(idKviza: Int): List<Odgovor>? {
            var kvizTakenFinal: KvizTaken? = null
            var responseKvizTakenList = ApiAdapter.retrofit.getListKvizTaken();
            var idKvizaPom = idKviza
            for (i in responseKvizTakenList.body()!!) {
                if (i.KvizId == idKviza) idKvizaPom = i.id
            }
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getOdgovoriKviz(idKvizaPom);
                return@withContext response.body()
            }
        }

        suspend fun postaviOdgovorKviz(idKvizTaken: Int, idPitanje: Int, odgovor: Int): Int? {
            var kvizTakenFinal: KvizTaken? = null
            var responseKvizTakenList = ApiAdapter.retrofit.getListKvizTaken();

            for (i in responseKvizTakenList.body()!!) {
                if (i.id == idKvizTaken) kvizTakenFinal = i
            }

            var responsePitanja = ApiAdapter.retrofit.getPitanja(kvizTakenFinal?.KvizId);
            var trenutnoPitanje: Pitanje? = null

            for (i in responsePitanja.body()!!) {
                if (i.id == idPitanje) trenutnoPitanje = i
            }

            if (trenutnoPitanje?.tacan == odgovor) {
                var pom = kvizTakenFinal?.osvojeniBodovi
                kvizTakenFinal?.osvojeniBodovi = pom!! + (100 / responsePitanja.body()!!.size)
                return withContext(Dispatchers.IO) {
                    var odg =
                        OdgovorPost(trenutnoPitanje.id, trenutnoPitanje.id, trenutnoPitanje.id)

                    var response = ApiAdapter.retrofit.postOdgovor(idKvizTaken, odg);
                    return@withContext kvizTakenFinal?.osvojeniBodovi
                }
                return kvizTakenFinal?.osvojeniBodovi
            }

            return -1
        }
    }


}