package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.kvizovi
import ba.etf.rma21.projekat.data.pitanja
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.pitanjaKviz
import ba.etf.rma21.projekat.view.ApiAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class PitanjeKvizRepository {


    companion object {

        init {

        }

        suspend fun getPitanja(idKviza: Int): List<Pitanje>? {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getPitanja(idKviza);
                return@withContext response.body()
            }
        }

        fun getPitanja(nazivKviza: String, nazivPredmeta: String): List<Pitanje>{
            var pitanjaFinal = listOf<Pitanje>()
            var nazivPitanja = listOf<String>()

            // moja je pretpostavka da je nazivPredmeta nepotreban u ovoj metodi ali s obzirom da je dodijeljena u dokumentu
            // koristit cu ga samo da bih provjerio da li postoji kviz vezan za naziv predmeta ukoliko postoji prebacit cu se na PitanjeKviz gdje cu uzeti sva pitanja vezana za taj kviz
            // i zatim cu proci kroz pitanjakviz i ukoliko se poklapaju sa nekim nazivom pitanja onda cu to pitanje ubaciti u konacnu listu

            var postoji: Boolean = false

            for(i in kvizovi()){
//                if(i.nazivPredmeta == nazivPredmeta && i.naziv == nazivKviza) {
//                    postoji = true
//                    break
               // }
            }

            if(postoji == true){
                for(i in pitanjaKviz()){
//                    if (nazivKviza == i.kviz)
//                        nazivPitanja += i.naziv
                }

                for(i in pitanja()){
//                    for(j in nazivPitanja){
//                        if(j == i.naziv)
//                            pitanjaFinal += i
//                    }
                }
            }

            return pitanjaFinal
        }

    }
}