package ba.etf.rma21.projekat.data.repositories

import AccountDAO
import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.view.ApiAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class PredmetIGrupaRepository {
    companion object{
        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }
        suspend fun getPredmeti():List<Predmet>? {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getPredmeti();
                return@withContext response.body()
            }
        }
        suspend fun getGrupe():List<Grupa>?{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getGrupe();
                return@withContext response.body()
            }
        }

        suspend fun getGrupeZaPredmet(idPredmeta:Int):List<Grupa>?{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getGrupeZaPredmet(idPredmeta);
                return@withContext response.body()
            }
        }

        suspend fun upisiUGrupu(idGrupa:Int):Boolean?{

//            var db = AppDatabase.getInstance(context)
//
//            var new = Account(AccountRepository.getHash(), "ss")
//            db.accDao().insertAcc()
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.upisiUGrupu(idGrupa);
                if(response.isSuccessful) return@withContext true;
                return@withContext false
            }
        }

        suspend fun getUpisaneGrupe():List<Grupa>?{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getUpisaneGrupe()
                return@withContext response.body()
            }
        }


    }
}