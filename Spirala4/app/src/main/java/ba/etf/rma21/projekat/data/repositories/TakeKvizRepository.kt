package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.view.ApiAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TakeKvizRepository {
    companion object {

        suspend fun zapocniKviz(idKviza: Int): KvizTaken? {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getKvizTaken( idKviza);
                return@withContext response.body()
            }
        }


        suspend fun getPocetiKvizovi(): List<KvizTaken>? {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getListKvizTaken();
                if(response.body()?.size == 0) return@withContext null
                return@withContext response.body()
            }
        }
    }

}

