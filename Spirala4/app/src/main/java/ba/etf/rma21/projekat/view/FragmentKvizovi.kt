package ba.etf.rma21.projekat.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.MainActivity.Companion.korisnik
import ba.etf.rma21.projekat.MarginItemDecoration
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView

class FragmentKvizovi : Fragment() {
    private lateinit var kvizovi: RecyclerView
    private lateinit var kvizoviAdapter: KvizListAdapter
    private lateinit var filterKvizova: Spinner
    private var kvizListViewModel = KvizListViewModel()


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                if (filterKvizova.selectedItem.toString() == "Svi moji kvizovi") {
                    kvizoviAdapter.updateKvizovi(MainActivity.korisnik.kvizovi)
                } else if (filterKvizova.selectedItem.toString() == "Svi kvizovi") {
                    kvizoviAdapter.updateKvizovi(kvizListViewModel.getSviKvizovi())
                } else if (filterKvizova.selectedItem.toString() == "Urađeni kvizovi") {
                    kvizoviAdapter.updateKvizovi(kvizListViewModel.getUradjeniKvizovi())
                } else if (filterKvizova.selectedItem.toString() == "Budući kvizovi") {
                    kvizoviAdapter.updateKvizovi(kvizListViewModel.getBuduciKvizovi())
                } else {
                    kvizoviAdapter.updateKvizovi(kvizListViewModel.getProsliNeUradjeni())
                }

                filterKvizova.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {

                    }

                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        if (filterKvizova.selectedItem.toString() == "Svi moji kvizovi") {
                            kvizoviAdapter.updateKvizovi(MainActivity.korisnik.kvizovi)
                        } else if (filterKvizova.selectedItem.toString() == "Svi kvizovi") {
                            kvizoviAdapter.updateKvizovi(kvizListViewModel.getSviKvizovi())
                        } else if (filterKvizova.selectedItem.toString() == "Urađeni kvizovi") {
                            kvizoviAdapter.updateKvizovi(kvizListViewModel.getUradjeniKvizovi())
                        } else if (filterKvizova.selectedItem.toString() == "Budući kvizovi") {
                            kvizoviAdapter.updateKvizovi(kvizListViewModel.getBuduciKvizovi())
                        } else {
                            kvizoviAdapter.updateKvizovi(kvizListViewModel.getProsliNeUradjeni())
                        }
                    }

                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
      var view =  inflater.inflate(R.layout.fragment_kvizovi, container, false)
        var view2 =  inflater.inflate(R.layout.item_kviz, container, false)
        super.onCreate(savedInstanceState)


        val navBar: BottomNavigationView = requireActivity().findViewById(R.id.bottomNavigation)

        navBar.menu.findItem(R.id.kvizovi).isVisible = true
        navBar.menu.findItem(R.id.predmeti).isVisible = true
        navBar.menu.findItem(R.id.predajKviz).isVisible = false
        navBar.menu.findItem(R.id.zaustaviKviz).isVisible = false

        kvizovi = view.findViewById(R.id.listaKvizova)
        kvizovi.layoutManager = GridLayoutManager(
            activity,
            2
        )
        filterKvizova = view.findViewById(R.id.filterKvizova)

        kvizoviAdapter = KvizListAdapter(listOf(), korisnik)
        kvizovi.adapter = kvizoviAdapter


        //Potrebno provjeriti spinner i na osnovu toga slati u adapter
        filterKvizova.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (filterKvizova.selectedItem.toString() == "Svi moji kvizovi") {
                    kvizoviAdapter.updateKvizovi(kvizListViewModel.getKvizovi())
                } else if (filterKvizova.selectedItem.toString() == "Svi kvizovi") {
                    kvizoviAdapter.updateKvizovi(kvizListViewModel.getSviKvizovi())
                } else if (filterKvizova.selectedItem.toString() == "Urađeni kvizovi") {
                    kvizoviAdapter.updateKvizovi(kvizListViewModel.getUradjeniKvizovi())
                } else if (filterKvizova.selectedItem.toString() == "Budući kvizovi") {
                    kvizoviAdapter.updateKvizovi(kvizListViewModel.getBuduciKvizovi())
                } else {
                    kvizoviAdapter.updateKvizovi(kvizListViewModel.getProsliNeUradjeni())
                }

            }

        }

        navBar.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.kvizovi -> {
                    val transaction: FragmentTransaction =
                        (context as FragmentActivity).supportFragmentManager.beginTransaction()
                    transaction.replace(
                        R.id.container,
                        FragmentKvizovi()
                    )
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    transaction.addToBackStack(null)
                    transaction.commit()
                    true
                }
                R.id.predmeti -> {
                    val transaction: FragmentTransaction =
                        (context as FragmentActivity).supportFragmentManager.beginTransaction()
                    transaction.replace(
                        R.id.container,
                        FragmentPredmeti()
                    )
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    transaction.addToBackStack(null)
                    transaction.commit()
                    false
                }
                else -> {

                    false
                }

            }
        }

        kvizovi.addItemDecoration(
            MarginItemDecoration(10)
        )

        return view;
    }
    companion object {
        fun newInstance(): FragmentKvizovi = FragmentKvizovi()
    }
}