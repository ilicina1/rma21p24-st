package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.OdgovorenaPitanja
import ba.etf.rma21.projekat.data.models.Pitanje
import com.google.android.material.bottomnavigation.BottomNavigationView

class FragmentPitanje(val pitanje: Pitanje) : Fragment() {
    private lateinit var textPitanje: TextView
    private lateinit var odgovoriLista: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_pitanje, container, false)
        val context = context as MainActivity

        textPitanje = view.findViewById(R.id.tekstPitanja)
        odgovoriLista = view.findViewById(R.id.odgovoriLista)

//        val adapter = ArrayAdapter(context, android.R.layout.simple_list_item_1, 0)
//        odgovoriLista.adapter = adapter
        textPitanje.setText(pitanje.naziv)

        var pom: Boolean = false
        var pom2 = 0
        for (i in MainActivity.korisnik.odgovorenaPitanja) {
            if (i.nazivPitanja == pitanje.naziv) {
                pom = true
                pom2 = i.odabran
            }
        }


        if (pom == true) {
            odgovoriLista.setEnabled(false)
            odgovoriLista.performClick();

//            odgovoriLista.setAdapter(object : ArrayAdapter<String?>(
//                context,
//                android.R.layout.simple_list_item_1,
//                pitanje.opcije
//            ) {
//                override fun getView(
//                    position: Int,
//                    convertView: View?,
//                    parent: ViewGroup
//                ): View {
//                    val row = super.getView(position, convertView, parent)
//
//                    if (position == pom2 && pom2 == pitanje.tacan) {
//                        row.setBackgroundColor(getResources().getColor(R.color.zelena))
//                    }
//                    if (position == pom2 && pom2 != pitanje.tacan) {
//                        row.setBackgroundColor(getResources().getColor(R.color.crvena))
//                    }
//                    if (position == pitanje.tacan)
//                        row.setBackgroundColor(getResources().getColor(R.color.zelena))
//
//                    return row
//                }
//            })
        }
        odgovoriLista.setOnItemClickListener { parent, view, position, id ->
            if (position == pitanje.tacan) {
                odgovoriLista.get(position)
                    .setBackgroundColor(getResources().getColor(R.color.zelena))
                odgovoriLista.setEnabled(false)
            } else {
                odgovoriLista.get(position)
                    .setBackgroundColor(getResources().getColor(R.color.crvena))
                odgovoriLista.get(pitanje.tacan)
                    .setBackgroundColor(getResources().getColor(R.color.zelena))
                odgovoriLista.setEnabled(false)
            }

            MainActivity.korisnik.odgovorenaPitanja += OdgovorenaPitanja(
                pitanje.naziv,
                pitanje.tacan,
                position
            )
        }


        val navBar: BottomNavigationView = requireActivity().findViewById(R.id.bottomNavigation)
        navBar.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.predajKviz -> {
                    val transaction: FragmentTransaction =
                        (context as FragmentActivity).supportFragmentManager.beginTransaction()
                    transaction.replace(
                        R.id.container,
                        FragmentPoruka("null", "null")
                    )
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    transaction.addToBackStack(null)
                    transaction.commit()
                    true
                }
                R.id.zaustaviKviz -> {
                    val transaction: FragmentTransaction =
                        (context as FragmentActivity).supportFragmentManager.beginTransaction()
                    transaction.replace(
                        R.id.container,
                        FragmentKvizovi()
                    )
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    transaction.addToBackStack(null)
                    transaction.commit()
                    false
                }
                else -> {

                    false
                }

            }
        }

        return view
    }

    companion object {
    }
}