package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView


class FragmentPokusaj(val lista: List<Pitanje>) : Fragment() {
    private lateinit var leftNavigation: NavigationView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_pokusaj, container, false)
        val navBar: BottomNavigationView = requireActivity().findViewById(R.id.bottomNavigation)
        leftNavigation = view.findViewById(R.id.navigacijaPitanja)

        navBar.menu.findItem(R.id.kvizovi).isVisible = false
        navBar.menu.findItem(R.id.predmeti).isVisible = false
        navBar.menu.findItem(R.id.predajKviz).isVisible = true
        navBar.menu.findItem(R.id.zaustaviKviz).isVisible = true

        var counter = 1
        for (i in lista ){
            leftNavigation.menu.add(counter.toString())
            counter++
        }

        navBar.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.kvizovi -> {
                    val transaction: FragmentTransaction =
                        (context as FragmentActivity).supportFragmentManager.beginTransaction()
                    transaction.replace(
                        R.id.container,
                        FragmentKvizovi()
                    )
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    transaction.addToBackStack(null)
                    transaction.commit()
                    true
                }
                R.id.predmeti -> {
                    val transaction: FragmentTransaction =
                        (context as FragmentActivity).supportFragmentManager.beginTransaction()
                    transaction.replace(
                        R.id.container,
                        FragmentPredmeti()
                    )
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    transaction.addToBackStack(null)
                    transaction.commit()
                    false
                }
                R.id.predajKviz -> {
                val transaction: FragmentTransaction =
                    (context as FragmentActivity).supportFragmentManager.beginTransaction()
                transaction.replace(
                    R.id.container,
                    FragmentPoruka("null", "null")
                )
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                transaction.addToBackStack(null)
                transaction.commit()
                true
                }
                R.id.zaustaviKviz -> {
                    val transaction: FragmentTransaction =
                        (context as FragmentActivity).supportFragmentManager.beginTransaction()
                    transaction.replace(
                        R.id.container,
                        FragmentKvizovi()
                    )
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    transaction.addToBackStack(null)
                    transaction.commit()
                    false
                }
                else -> {

                    false
                }

            }
        }

        leftNavigation.setNavigationItemSelectedListener {
            when (it.itemId) {
                else -> {
                    val transaction: FragmentTransaction =
                        (context as FragmentActivity).supportFragmentManager.beginTransaction()
                    transaction.replace(
                        R.id.framePitanje,
                        FragmentPitanje(lista.get(it.toString().toInt() - 1))
                    )
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    transaction.addToBackStack(null)
                    transaction.commit()
                    false
                }
            }
        }

        return view
    }

    companion object {

    }
}