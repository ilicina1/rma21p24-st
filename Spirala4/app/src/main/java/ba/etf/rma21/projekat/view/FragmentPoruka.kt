package ba.etf.rma21.projekat.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Pitanje

class FragmentPoruka(val grupa: String, val predmet: String) : Fragment() {
    private lateinit var tvPoruka: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_poruka, container, false)

        tvPoruka = view.findViewById(R.id.tvPoruka)

        fun onSuccess(pitanja: List<Pitanje>) {
            print(pitanja);
        }

        fun onError() {
//            val toast = Toast.makeText(context, "Search error", Toast.LENGTH_SHORT)
            print("blabl")
        }

//        PitanjeKvizViewModel().getPit(
//            onSuccess = ::onSuccess,
//            onError = ::onError
//        );


        if (grupa == "null" && predmet == "null")
            tvPoruka.setText("Završili ste kviz!");
        else
            tvPoruka.setText("Uspješno ste upisani u grupu $grupa predmeta $predmet");

        return view;
    }

    companion object {
    }
}