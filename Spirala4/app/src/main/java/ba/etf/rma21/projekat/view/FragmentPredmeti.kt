package ba.etf.rma21.projekat.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.kvizovi
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Korisnik
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.predmeti
import ba.etf.rma21.projekat.data.repositories.GrupaRepository
import com.google.android.material.bottomnavigation.BottomNavigationView

class FragmentPredmeti : Fragment() {
    private lateinit var odabirGodina: Spinner
    private lateinit var odabirPredmet: Spinner
    private lateinit var odabirGrupa: Spinner
    private lateinit var dodajPredmetDugme: Button
    private lateinit var pom: TextView


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_predmeti, container, false)
        super.onCreate(savedInstanceState)

        val navBar: BottomNavigationView = requireActivity().findViewById(R.id.bottomNavigation)

        navBar.menu.findItem(R.id.kvizovi).isVisible = true
        navBar.menu.findItem(R.id.predmeti).isVisible = true
        navBar.menu.findItem(R.id.predajKviz).isVisible = false
        navBar.menu.findItem(R.id.zaustaviKviz).isVisible = false

        val korisnik = activity?.intent?.getSerializableExtra("Korisnik") as? Korisnik

        odabirGodina = view.findViewById(R.id.odabirGodina)
        odabirPredmet = view.findViewById(R.id.odabirPredmet)
        odabirGrupa = view.findViewById(R.id.odabirGrupa)
        dodajPredmetDugme = view.findViewById(R.id.dodajPredmetDugme)



        odabirGodina.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var predmetiZaSpinner = listOf<String>()

                for (i in predmeti()) {
                    if (MainActivity.korisnik != null) {
                        var pomocna = false
                        for (j in (0..MainActivity.korisnik.upisaniPredmeti.size - 1)) {
                            if (i.naziv == MainActivity.korisnik.upisaniPredmeti.get(j).naziv) {
                                pomocna = true;
                            }
                        }
                        if (!pomocna && i.godina == odabirGodina.selectedItem.toString().toInt()){
                            predmetiZaSpinner += i.naziv
                        }
                    }
                }

                if (!predmetiZaSpinner.isEmpty()) {
                    val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                        requireContext(),
                        android.R.layout.simple_spinner_dropdown_item,
                        predmetiZaSpinner
                    )
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                    odabirPredmet.setAdapter(adapter)
                    adapter.notifyDataSetChanged()
                } else {
                    val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                        requireContext(),
                        android.R.layout.simple_spinner_dropdown_item,
                        listOf()
                    )
                    odabirPredmet.setAdapter(adapter)
                    adapter.notifyDataSetChanged()
                }

                if (odabirPredmet.selectedItem != null) {
                    if (!GrupaRepository.getGroupsByPredmetString(odabirPredmet.selectedItem.toString())
                            .isEmpty()
                    ) {
                        val adapterOdabirGrupe: ArrayAdapter<String> = ArrayAdapter<String>(
                            requireContext(),
                            android.R.layout.simple_spinner_dropdown_item,
                            GrupaRepository.getGroupsByPredmetString(odabirPredmet.selectedItem.toString())
                        )

                        adapterOdabirGrupe.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        odabirGrupa.setAdapter(adapterOdabirGrupe)
                        adapterOdabirGrupe.notifyDataSetChanged()
                    }
                } else {
                    val adapterOdabirGrupe: ArrayAdapter<String> = ArrayAdapter<String>(
                        requireContext(),
                        android.R.layout.simple_spinner_dropdown_item,
                        listOf()
                    )

                    odabirGrupa.setAdapter(adapterOdabirGrupe)
                    adapterOdabirGrupe.notifyDataSetChanged()
                }

                dodajPredmetDugme.isEnabled = odabirPredmet.selectedItem != null

            }


        }

        if (MainActivity.korisnik != null) {
            dodajPredmetDugme.setOnClickListener() {
                val value: String = "sendBackData"
                val intentNew = Intent()

                val grupa = odabirGrupa.selectedItem.toString()
                val predmet = odabirPredmet.selectedItem.toString()

                MainActivity.korisnik.upisaniPredmeti += Predmet(1,
                    odabirPredmet.selectedItem.toString(),
                    odabirGodina.selectedItem.toString().toInt()
                )
                MainActivity.korisnik.upisaniPredmetiGrupe += Grupa(1,
                    odabirGrupa.selectedItem.toString(),
                    odabirPredmet.selectedItem.toString()
                )

                for (i in kvizovi()) {
//                    if (i.nazivPredmeta == odabirPredmet.selectedItem.toString() && i.nazivGrupe == odabirGrupa.selectedItem.toString())
//                        MainActivity.korisnik.kvizovi += i
                }

                intentNew.putExtra("Korisnik", MainActivity.korisnik)
                activity?.setResult(Activity.RESULT_OK, activity?.intent)

                val transaction: FragmentTransaction =
                    (context as FragmentActivity).supportFragmentManager.beginTransaction()
                transaction.replace(
                    R.id.container,
                    FragmentPoruka(grupa, predmet)
                )
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                transaction.addToBackStack(null)
                transaction.commit()

            }
        }

        return view;
    }

    companion object {
        fun newInstance(): FragmentPredmeti = FragmentPredmeti()
    }
}