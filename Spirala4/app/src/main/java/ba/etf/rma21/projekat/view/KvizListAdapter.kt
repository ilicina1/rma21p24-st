package ba.etf.rma21.projekat.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Korisnik
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository.Companion.getPitanja
import java.text.SimpleDateFormat
import java.util.*


class KvizListAdapter(private var kvizovi: List<Kviz>, private var korisnik: Korisnik) :
    RecyclerView.Adapter<KvizListAdapter.KvizViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KvizViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_kviz, parent, false)

        return KvizViewHolder(view)
    }

    override fun getItemCount(): Int = kvizovi.size

    val format = SimpleDateFormat("dd.MM.yyy")

    override fun onBindViewHolder(holder: KvizViewHolder, position: Int) {

        val calNull = Calendar.getInstance()
        calNull[Calendar.YEAR] = 1999
        calNull[Calendar.MONTH] = Calendar.JANUARY
        calNull[Calendar.DAY_OF_MONTH] = 1
        val dateNull = calNull.time

//        holder.nazivPredmeta.text = kvizovi[position].nazivPredmeta;
        holder.nazivKviza.text = kvizovi[position].naziv;
        holder.minute.text = kvizovi[position].trajanje.toString()

        val context: Context = holder.stanje.getContext()

//        if (kvizovi[position].datumPocetka.after(Date())) {
//            var id: Int =
//                context.getResources().getIdentifier("zuta", "drawable", context.getPackageName())
//            holder.stanje.setImageResource(id)
//            holder.datum.text = format.format(kvizovi[position].datumPocetka)
//            holder.bodovi.text = " "
//        } else if ((kvizovi[position].datumPocetka.before(Date()) || kvizovi[position].datumPocetka.equals(
//                Date()
//            )) && kvizovi[position].datumKraj.after(Date()) && kvizovi[position].datumRada == null
//        ) {
//            var id: Int =
//                context.getResources().getIdentifier("zelena", "drawable", context.getPackageName())
//            holder.stanje.setImageResource(id)
//            holder.datum.text = format.format(kvizovi[position].datumKraj)
//            holder.bodovi.text = " "
//        } else if (kvizovi[position].datumKraj.before(Date()) && kvizovi[position].datumRada == null) {
//            var id: Int =
//                context.getResources().getIdentifier("crvena", "drawable", context.getPackageName())
//            holder.stanje.setImageResource(id)
//            holder.datum.text = format.format(kvizovi[position].datumKraj)
//            holder.bodovi.text = " "
//        } else if (kvizovi[position].datumRada != null) {
//            var id: Int =
//                context.getResources().getIdentifier("plava", "drawable", context.getPackageName())
//            holder.stanje.setImageResource(id)
//            holder.datum.text = format.format(kvizovi[position].datumRada)
//            holder.bodovi.text = kvizovi[position].osvojeniBodovi.toString()
//        }
//
//        holder.itemView.setOnClickListener {
//            if((kvizovi[position].datumPocetka.before(Date()) || kvizovi[position].datumPocetka.equals(
//                    Date()
//                )) && kvizovi[position].datumKraj.after(Date()) && kvizovi[position].datumRada == null) {
//                val transaction: FragmentTransaction =
//                    (context as FragmentActivity).supportFragmentManager.beginTransaction()
//                transaction.replace(
//                    R.id.container,
//                    FragmentPokusaj(
//                        getPitanja(
//                            holder.nazivKviza.text.toString(),
//                            holder.nazivPredmeta.text.toString()
//                        )
//                    )
//                )
//
//                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                transaction.addToBackStack(null)
//                transaction.commit()
//            }
//
//        }
    }


    fun updateKvizovi(kvizovi: List<Kviz>) {
        this.kvizovi = kvizovi.sortedBy { it.datumPocetka }
        notifyDataSetChanged()
    }

    inner class KvizViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nazivPredmeta: TextView = itemView.findViewById(R.id.nazivPredmeta)
        val nazivKviza: TextView = itemView.findViewById(R.id.nazivKviza)
        val datum: TextView = itemView.findViewById(R.id.datum)
        val minute: TextView = itemView.findViewById(R.id.minute)
        val bodovi: TextView = itemView.findViewById(R.id.bodovi)
        val stanje: ImageView = itemView.findViewById(R.id.stanje)

        init {
        }
    }
}