package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.repositories.KvizRepository

class KvizListViewModel {
    fun getKvizovi(): List<Kviz> {
        return KvizRepository.getMyKvizes();
    }

    fun getSviKvizovi(): List<Kviz> {
        return KvizRepository.getAlla()
    }

    fun getUradjeniKvizovi(): List<Kviz> {
        return KvizRepository.getDone()
    }

    fun getBuduciKvizovi(): List<Kviz> {
        return KvizRepository.getFuture()
    }

    fun getProsliNeUradjeni(): List<Kviz> {
        return KvizRepository.getNotTaken()
    }

}